#!/usr/bin/env python
#-*- coding: utf-8 -*-

"""
Retreive data from ECMWF facilities transfering the files
to the your remote data receiver.

For a specific run time or period of time (defined by the user)
this script does:

    i) a request to MARS arquive (at ECMWF) to retreive data, specifying
        variable(s), area, model time steps, and some other mars request parameters.
    ii) transfer the files to the remote location defined in the <ECASSOC>
        that must be previously created in the "ECMWF ecaccess service".
    iii) netCDF convertion (using CDO...).
    
    This script may be used in:
    a) the linux shell command line of your account at the ECMWF servers (ecgate)
    b) sumbited as a job in the "ECMWF ecaccess service"

    In the latest case the #Batch request script# MUST include some
    lines before calling the script (see Usage Ex3 bellow):



USAGE EXAMPLES  :

    Ex1 - Get Era-Interim (class=ei) skt data (-v skt) for 01/Jan/2015 (-i 20150101)
          in a Dayly agregation (-D) for the model steps 12 to 21 with 3 hourly time
          resolution (-s 12 21 3) and 0.75 grid spatial resolution
          (--grid_resolution 0.75 0.75), in netCDF4 file format (-NC) :

        ./getEcData.py -i 20150101 -D -s 12 21 3 -p $HOME/DATA -o ERA-Int_SKT_%Y%m%d --additional_mars_args class=ei expver=1 type=fc levtyp=sfc repres=gg --grid_resolution 0.75 0.75 -v skt
 -NC

    Ex2 - Get ECMWF forecast data generated between 10/Jan/2011 and 18/Jan/2011,
          in separated files (one for each run/step) and transfer data to <my_assciation> :

        ./getEcData.py -i 20110110 -f 20110118 -t my_association -S -p $HOME/DATA


    Ex3 - Get the latest forecasted data generated at run time 12UTC (trigger: 179 - fc12h036)
          and transfer data to <my_association>, assuming that getEcData.py script exists in
          your ecgate home dir.
          Using ECMWF ecaccess service to submit the following job :

#
#  Batch request script:
#

#@ shell = /usr/bin/ksh
#@ class = express
#@ environment = COPY_ALL
#@ queue
set -exva

scratch_dir=$SCRATCH
event_time=$MSJ_BASETIME
event_date=${MSJ_YEAR}${MSJ_MONTH}${MSJ_DAY}
$HOME/getEcData.py -i $event_date -r $event_time -p $scratch_dir -t my_association

"""

import argparse
import os
import datetime as dt
import subprocess
import uuid
from calendar import monthrange

class GetEcData(object):
    def __init__(self, initial_timeslot, final_timeslot="",
                 run_time=[0,12], var_list=["2T", "2D", "TCWV", "SKT"],
                 step_range=[12,36,1], association=None,
                 step_list=[], area=[90.0,-180.0,-90.0,180.0],
                 grid_resolution=[.125,.125], path="", additional_mars_args=\
                 ["levtype=sfc","type=fc","repres=gg"],
                 netcdf4_output = False, nc4_compression_level = 9,
                 daily_data = False,
                 monthly_cycle = False,
                 step_by_step = True,
                 output_file_pattern = "GRIB_F-%K_ECMWF_All_Globe_%Y%m%d%L00"):

        self.start_dt = dt.datetime.strptime(initial_timeslot, "%Y%m%d")
        if monthly_cycle:
            n_days = monthrange(self.start_dt.year,self.start_dt.month)[1] -\
                     self.start_dt.day
            self.end_dt = self.start_dt + dt.timedelta(days=n_days)
            print "final date: {}".format(self.end_dt)
        elif any(final_timeslot):
            self.end_dt = dt.datetime.strptime(final_timeslot, "%Y%m%d")
        else:
            self.end_dt = self.start_dt
        self.runtime_list = run_time
        self.parvar = var_list
        self.step_range = step_range
        self.ecassoc = association
        self.step_list = step_list
        self.area = area
        area[2] += grid_resolution[0]
        area[3] -= grid_resolution[1]
        self.grid = grid_resolution
        self.path = path
        self.add_args_list = additional_mars_args
        self.output_file_pattern = output_file_pattern
        self.output_format_nc4 = netcdf4_output
        self.nc4_compression_level = nc4_compression_level
        self.daily_data = daily_data
        self.step_by_step = step_by_step

    def one_request(self, timeslot_dt, run_time, step_list, out_timeslot_dt=None):
        step_str = "-".join(["{:03d}".format(int(s)) for s in step_list])
        if out_timeslot_dt is None:
            target_file_name = timeslot_dt.strftime(self.output_file_pattern)
        else:
            target_file_name = out_timeslot_dt.strftime(self.output_file_pattern)
        target_file_name = target_file_name.replace("%K","{}".format(step_str))
        target_file_name = target_file_name.replace("%L","{:02d}".format(int(run_time)))
        step_str = "/".join(["{:03d}".format(int(s)) for s in step_list])
        mars_request = "ret,\n"\
                       " {},\n"\
                       " grid={},\n"\
                       " area={},\n"\
                       " param={},\n"\
                       " date={},\n"\
                       " step={},\n"\
                       " time={:02d},\n"\
                       " target={}\n".format(
                           ",\n ".join(self.add_args_list),
                           "/".join([str(el) for el in self.grid]),
                           "/".join([str(el) for el in self.area]),
                           "/".join(self.parvar),
                           timeslot_dt.strftime("%Y%m%d"),
                           step_str,
                           run_time,
                           target_file_name
                       )
        return mars_request

    def submit_request(self,  timeslot_dt, run_time, step_list, mars_request):
        step_str = "-".join(["{:03d}".format(int(step)) for step in step_list])
        target_file_name = timeslot_dt.strftime(self.output_file_pattern)
        target_file_name = target_file_name.replace("%K","{}".format(step_str))
        target_file_name = target_file_name.replace("%L","{:02d}".format(int(run_time)))
        time_id = "{}_{}{:02d}".format(
            step_str, timeslot_dt.strftime("%Y%m%d"),
            run_time
        )
        temp_dir = os.path.join(self.path,"{}_{}".format(
                       str(uuid.uuid4()),
                       time_id))

        # Create a local temporary storage directory:
        #--------------------------------------------
        os.makedirs(temp_dir)

        # Create a temporary file holding the mars request directives:
        #-------------------------------------------------------------
        request_file_name = os.path.join(
            temp_dir,
            "request_{}".format(time_id)
        )
        with open(request_file_name, 'w') as request_file:
            request_file.write(mars_request)

        # Get data from MARS to the local temporary storage:
        #---------------------------------------------------
        mars_proc = subprocess.Popen("mars {}".format(request_file_name),
                                     cwd=temp_dir, shell=True,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
        out, err = mars_proc.communicate()
        print "{}\n{}".format(out, err)

        # Use cdo to change file format into
        # a netCDF4 with internal compression:
        #-------------------------------------
        if self.output_format_nc4:
            nc4_proc = subprocess.Popen("module load cdo;"
                                        "cdo -t ecmwf -f nc4 -r -z zip_{0} "
                                        "copy {1} {1}.nc".format(self.nc4_compression_level,
                                                                 target_file_name),
                                     cwd=temp_dir, shell=True,
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
            out, err = nc4_proc.communicate()
            print "{}\n{}".format(out, err)
            target_file_name = "{}.nc".format(target_file_name)

        print "output file: {}".format(os.path.join(temp_dir,target_file_name))

        if self.ecassoc is None:
            for tmp_file in os.listdir(temp_dir):
                if os.path.basename(tmp_file) != target_file_name:
                    os.remove(os.path.join(temp_dir,tmp_file))
                else:
                    os.rename(os.path.join(temp_dir,tmp_file),
                              os.path.join(self.path,tmp_file))
            os.rmdir(temp_dir)
        else:
            # Transfer data via IM gatway to the location defined
            # in the ectrans association:
            #----------------------------------------------------
            trans_proc = subprocess.Popen("ectrans -gateway ecgw.meteo.pt"
                                          " -remote {} -overwrite -priority 0"
                                          " -source {}".format(self.ecassoc,
                                                               target_file_name),
                                          cwd=temp_dir, shell=True,
                                          stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE)
            out, err = trans_proc.communicate()
            print "{}\n{}".format(out, err)

            # Remove the temporary data:
            #---------------------------
            for tmp_file in os.listdir(temp_dir):
                os.remove(os.path.join(temp_dir,tmp_file))
            os.rmdir(temp_dir)

    def request_all(self):
        date_now = self.start_dt
        step_list = []
        if len(self.step_list) > 0:
            step_list = self.step_list
        elif len(self.step_range) > 0:
            step_list = range(self.step_range[0],
                              self.step_range[1]+1,
                              self.step_range[2])
        while date_now <= self.end_dt:
            if self.daily_data:
                print("Join daily data...")
                mars_request = self.one_request(
                    date_now - dt.timedelta(days=1), 12, step_list,
                    date_now
                )
                mars_request = "{}{}".format(
                    mars_request,
                    self.one_request(
                        date_now, 0, step_list
                    )
                )
                print("MARS request:\n{}".format(mars_request))
                self.submit_request(date_now, 0, step_list, mars_request)
            else:
                for run_time in self.runtime_list:
                    if self.step_by_step:
                        for step in step_list:
                            mars_request = self.one_request(date_now, run_time, [step])
                            print("MARS request:\n{}".format(mars_request))
                            self.submit_request(date_now, run_time, [step], mars_request)
                    else:
                        mars_request = self.one_request(date_now, run_time, step_list)
                        print("MARS request:\n{}".format(mars_request))
                        self.submit_request(date_now, run_time, step_list, mars_request)
            date_now += dt.timedelta(days=1)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
                 formatter_class=argparse.RawDescriptionHelpFormatter,
                 description=__doc__
             )
    parser.add_argument('-i','--initial_timeslot', type=str, required=True,
                            help="Initial timeslot of the downloaded period."
                                 " (fmt:YYYYmmdd)")
    parser.add_argument('-f','--final_timeslot',  type=str, default="",
                        help="Final timeslot of the downloaded period."
                             " (fmt:YYYYmmdd)"
                             "The default value is the initial timeslot.")
    parser.add_argument('-r','--run_time', type=int, nargs='*',
                        default=[0, 12],
                        help="Runs can be one of (0, 12) or both.")
    parser.add_argument('-s','--step_range', type=int, nargs=3,
                        default=[12, 36, 1],
                        help="Steps range whith the "
                             "respective step interval, of the retrieved data."
                             "Arguments sequence: start, end, interval")
    parser.add_argument('-l','--step_list', type=int, nargs="*",
                        default=[],
                        help="List of the retrieved data steps. "
                             "If the data steps are contiguous, use the "
                             "'-s' flag to select the respective range.")
    parser.add_argument('-v','--var_list', type=str, nargs="*",
                        default=["2T", "2D", "TCWV", "SKT"],
                        help="Variables names list.")
    parser.add_argument('-t','--association', type=str,
                        default=None,
                        help="ECtrans association name as defined in the "
                             "\"ECMWF ecaccess service\". This flag "
                             "automatically enables the ectransfer of the "
                             "retrieved files."
                             "Adding associations can be done through the "
                             "ECaccess web interface of the gateway you "
                             "want to transfer to. (https://ecgw.meteo.pt:"
                             "9443)")
    parser.add_argument('-a','--area', type=int, nargs=4,
                        default=[90, -180, -90, 180],
                        help="Area boundaries (uper left and lower right "
                             "corners) in latitude/longitude coordinates. "
                             "Default value is: 90 -180 -90 180")
    parser.add_argument('-g','--grid_resolution', type=float, nargs=2,
                        default=[.125,.125], help="Grid resolution of the "
                        "retrieved data.")
    parser.add_argument('-m','--additional_mars_args', type=str, nargs="*",
                        default=["LEVTYPE=SFC","TYPE=FC",
                                 "REPRES=GG","CLASS=OD",
                                 "STREAM=OPER","EXPVER=1"],
                        help="List of MARS aditional keywords "
                             "with the respective values. "
                             "Default value: TYPE=FC LEVTYPE=SFC REPRES=GG "
                             "CLASS=OD STREAM=OPER EXPVER=1")
    parser.add_argument('-p','--path', type=str, default="",
                        help="Path of the temporary local"
                             "storage before the data transfer.")
    parser.add_argument('-o','--output_file_pattern', type=str,
                        default="GRIB_F-%K_ECMWF_All_Globe_%Y%m%d%L00",
                        help="Output file pattern, datetime format like. "
                             "Also %%K and %%L can be used to replace step and "
                             "run time respectively. "
                             "Default value: "
                             "GRIB_F-%%K_ECMWF_All_Globe_%%Y%%m%%d%%L00")
    parser.add_argument('-D','--daily_data', action='store_true',
                        dest='daily_data',
                        help="Download run time 0 of the current day "
                             "and run time 12 for the day before and store "
                             "both runs time steps in the same file. "
                             "Given time steps shoud be values between 12 "
                             "and 23 to ensure all data are reltive to the"
                             "current day."
                             "(This flag overwrites the '-S' option.)")
    parser.set_defaults(daily_data = False)
    parser.add_argument('-S','--step_by_step', action='store_true',
                        dest='step_by_step',
                        help="Download each time step in a different "
                             "file. The default behavior is to store "
                             "all the time steps of the respective run time "
                             "in the same file. "
                             "(This flag is overwriten by the "
                             "'-D' option.)")
    parser.set_defaults(step_by_step = False)
    parser.add_argument('-NC','--netcdf4_output', action='store_true',
                        dest='netcdf4_output',
                        help="Change output file format "
                             "into a netCDF4. This operation is "
                             "performed by the cdo tool. Thus "
                             "it's performance depends on how "
                             "the cdo deals with the original "
                             "file format.")
    parser.set_defaults(netcdf4_output = False)
    parser.add_argument('-M','--monthly_cycle', action='store_true',
                        dest='monthly_cycle',
                        help="Cycle until the end of the month, "
                             "starting in the day defined with "
                             "the '-i' flag, discarding the '-f' "
                             "flag to define the end of the retrieved "
                             "periode.")
    parser.set_defaults(monthly_cycle = False)

    parser.add_argument('-c','--nc4_compression_level', type=int,
                        default=9, help="Set netCDF4 deflate compression level. "
                                        "For this option to take effect the "
                                        "'-D' flag must be used and set True.")
    args = parser.parse_args()

    #print("Arguments list:\n",dict(args._get_kwargs()))
    get_obj = GetEcData(**dict(args._get_kwargs()))
    get_obj.request_all()
