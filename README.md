# getEcData:
Python script to retrieve data from ECMWF facilities (MARS) and transfer the retrieved data to the your previously configured remote data receiver (ECtrans association).

### Main features:

    i) sends data requests to MARS arquive (at ECMWF) to retreive data, specifying
        variable(s), area, model run time, model time steps, and some other mars request parameters.
    ii) transfers the retrieved files to remote locations using previously created/configured ECtrans associations <ECASSOC>.
    iii) data format convertion to netCDF4.
    
    This script may be used in the two following contexts:
    a) linux shell command line of your account at the ECMWF servers (ecgate)
    b) sumbited as a job in the "ECMWF ecaccess service"

    In the latest case the "Batch request script" MUST include some
    lines before calling the script (see bellow the usage example #3):



### Usage examples  :

    1) - Get Era-Interim (class=ei) skt data (-v skt) for 01/Jan/2015 (-i 20150101)
          in a Dayly agregation (-D) for the model steps 12 to 21 with 3 hourly time
          resolution (-s 12 21 3) and 0.75 grid spatial resolution
          (--grid_resolution 0.75 0.75), in netCDF4 file format (-NC) :

        ./getEcData.py -i 20150101 -D -s 12 21 3 -p $HOME/DATA -o ERA-Int_SKT_%Y%m%d --additional_mars_args class=ei expver=1 type=fc levtyp=sfc repres=gg --grid_resolution 0.75 0.75 -v skt -NC

    2) - Get ECMWF forecast data generated between 10/Jan/2011 and 18/Jan/2011,
          in separated files (one for each run/step) and transfer data to <my_assciation> :

        ./getEcData.py -i 20110110 -f 20110118 -t my_association -S -p $HOME/DATA

    3) - Get the latest forecasted data generated at run time 12UTC (trigger: 179 - fc12h036)
          and transfer data to <my_association>, assuming that getEcData.py script exists in
          your ecgate home dir.

### ecaccess service example to submit job :
```
    #
    #  Batch request script:
    #

    #@ shell = /usr/bin/ksh
    #@ class = express
    #@ environment = COPY_ALL
    #@ queue
    set -exva

    scratch_dir=$SCRATCH
    event_time=$MSJ_BASETIME
    event_date=${MSJ_YEAR}${MSJ_MONTH}${MSJ_DAY}
    $HOME/getEcData.py -i $event_date -r $event_time -p $scratch_dir -t my_association
```
```    
./getEcData/getEcData.py --help
usage: getEcData.py [-h] -i INITIAL_TIMESLOT [-f FINAL_TIMESLOT]
                    [-r [RUN_TIME [RUN_TIME ...]]]
                    [-s STEP_RANGE STEP_RANGE STEP_RANGE]
                    [-l [STEP_LIST [STEP_LIST ...]]]
                    [-v [VAR_LIST [VAR_LIST ...]]] [-t ASSOCIATION]
                    [-a AREA AREA AREA AREA]
                    [-g GRID_RESOLUTION GRID_RESOLUTION]
                    [-m [ADDITIONAL_MARS_ARGS [ADDITIONAL_MARS_ARGS ...]]]
                    [-p PATH] [-o OUTPUT_FILE_PATTERN] [-D] [-S] [-NC] [-M]
                    [-c NC4_COMPRESSION_LEVEL]
                    
optional arguments:
  -h, --help            show this help message and exit
  -i INITIAL_TIMESLOT, --initial_timeslot INITIAL_TIMESLOT
                        Initial timeslot of the downloaded period.
                        (fmt:YYYYmmdd)
  -f FINAL_TIMESLOT, --final_timeslot FINAL_TIMESLOT
                        Final timeslot of the downloaded period.
                        (fmt:YYYYmmdd)The default value is the initial
                        timeslot.
  -r [RUN_TIME [RUN_TIME ...]], --run_time [RUN_TIME [RUN_TIME ...]]
                        Runs can be one of (0, 12) or both.
  -s STEP_RANGE STEP_RANGE STEP_RANGE, --step_range STEP_RANGE STEP_RANGE STEP_RANGE
                        Steps range whith the respective step interval, of the
                        retrieved data.Arguments sequence: start, end,
                        interval
  -l [STEP_LIST [STEP_LIST ...]], --step_list [STEP_LIST [STEP_LIST ...]]
                        List of the retrieved data steps. If the data steps
                        are contiguous, use the '-s' flag to select the
                        respective range.
  -v [VAR_LIST [VAR_LIST ...]], --var_list [VAR_LIST [VAR_LIST ...]]
                        Variables names list.
  -t ASSOCIATION, --association ASSOCIATION
                        ECtrans association name as defined in the "ECMWF
                        ecaccess service". This flag automatically enables the
                        ectransfer of the retrieved files.Adding associations
                        can be done through the ECaccess web interface of the
                        gateway you want to transfer to.
                        (https://ecgw.meteo.pt:9443)
  -a AREA AREA AREA AREA, --area AREA AREA AREA AREA
                        Area boundaries (uper left and lower right corners) in
                        latitude/longitude coordinates. Default value is: 90
                        -180 -90 180
  -g GRID_RESOLUTION GRID_RESOLUTION, --grid_resolution GRID_RESOLUTION GRID_RESOLUTION
                        Grid resolution of the retrieved data.
  -m [ADDITIONAL_MARS_ARGS [ADDITIONAL_MARS_ARGS ...]], --additional_mars_args [ADDITIONAL_MARS_ARGS [ADDITIONAL_MARS_ARGS ...]]
                        List of MARS aditional keywords with the respective
                        values. Default value: TYPE=FC LEVTYPE=SFC REPRES=GG
                        CLASS=OD STREAM=OPER EXPVER=1
  -p PATH, --path PATH  Path of the temporary localstorage before the data
                        transfer.
  -o OUTPUT_FILE_PATTERN, --output_file_pattern OUTPUT_FILE_PATTERN
                        Output file pattern, datetime format like. Also %K and
                        %L can be used to replace step and run time
                        respectively. Default value:
                        GRIB_F-%K_ECMWF_All_Globe_%Y%m%d%L00
  -D, --daily_data      Download run time 0 of the current day and run time 12
                        for the day before and store both runs time steps in
                        the same file. Given time steps shoud be values
                        between 12 and 23 to ensure all data are reltive to
                        thecurrent day.(This flag overwrites the '-S' option.)
  -S, --step_by_step    Download each time step in a different file. The
                        default behavior is to store all the time steps of the
                        respective run time in the same file. (This flag is
                        overwriten by the '-D' option.)
  -NC, --netcdf4_output
                        Change output file format into a netCDF4. This
                        operation is performed by the cdo tool. Thus it's
                        performance depends on how the cdo deals with the
                        original file format.
  -M, --monthly_cycle   Cycle until the end of the month, starting in the day
                        defined with the '-i' flag, discarding the '-f' flag
                        to define the end of the retrieved periode.
  -c NC4_COMPRESSION_LEVEL, --nc4_compression_level NC4_COMPRESSION_LEVEL
                        Set netCDF4 deflate compression level. For this option
                        to take effect the '-D' flag must be used and set
                        True.
```